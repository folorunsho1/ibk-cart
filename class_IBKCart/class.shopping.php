<?php

	/*
		ClassName	=>	IBKCart Class
		Author		=>	Shina (Don't bite your tongue, just call me Nuel) .. email: ibk.com@gmail.com
		Date		=>	Jan 2010.
		Version		=>	1.0.0
		
		+++++++++++++++++++++++++++
		Newly corrected due to warning notices..
		Date:	03 Feb 10
		Thanks to Mr. Laurent Girard for the notice...
		++++++++++++++++++++++++++++++++
	*/


	class IBKCart{
	
		var $cart;
		var $prodInfo;
		var $totAmt;
		var $Conn;
		var $tmpConn;
		var $dbStat;
		var $DBName;
		var $tblNm;
		var $prodPrc;
		var $prodNm;
		var $prodId;
		var $prodImg;
		var $ImgDir;
		var $CurTyp;
		var $ChkOutPg;
		var	$CSS;

		//Newly added ..
		var $ProcessingPg;
		
		function initCart($Config, $CartArr = ""){
			$this->tmpConn	= mysql_connect($Config['host'],$Config['uNm'], $Config['pWd']);
						
			if ($this->tmpConn){
				$this->dbStat = mysql_select_db($Config['dbNm'], $this->tmpConn);
				
					if ($this->dbStat){
						$this->Conn = $this->tmpConn;
						$this->cart = $CartArr;
						$this->totAmt = 0;
						$this->ChkOutPg = $Config['ChkOutPg'];
						
						//Initialize the cart product table and image directory..
						if ($this->table_exists($Config['prodTbl'], $Config['dbNm'])){
							$this->SetTblDts($Config['dbNm'], $Config['prodTbl'], $Config['prodPrc'], $Config['prodNm'], $Config['prodId'], $Config['prodImg'], $Config['imgDir']);
							$this->CurTyp = $Config['Cur'];
							$this->CSS = $Config['CSS'];
							$this->ProcessingPg = $Config['ProcessingPg'];
						}
						else{
							//Error..table not found..
							$this->DisplayError(3);
						}
			
					}
					else{
						//..Call the function that displays the database error..
						$this->DisplayError(2);

					}
			}
			else{
				//..Call the function that displays the Mysql_Connect Error..
				$this->DisplayError(1);
			}
			
		}
	
		function DisplayError($Err){
			switch($Err){
				case 1: //MysSQL Server ..
					$msg = "Could not connect to the MySQL Server. Please check your connection setings.";
				break;
				case 2: //Database Error..
					$msg = "There was a problem connecting to the selected database. Please check your connection setings.";
				break;
				case 3: //Table wasn't found..
					$msg = "The product table specified was not found in the selected database. Please check your setings.";
				break;
				case 4:
					$msg = 'There was a query error';
				break;
			}
			
			echo "<div class='Error'>". $msg ."</div>";
		}
		
		function ShowCurSymbol(){
			$symb = ($this->CurTyp == 'Dollar') ?  "$" : "<del>N</del>"  ;
			return $symb;
		}
		
		function addItem($item_id, $Qty){
			if (!empty($this->cart[$item_id])){
				$this->cart[$item_id] += $Qty;
			}
			else{
				$this->cart[$item_id] = $Qty;
			}
		}
		
		function updateCart($item_id, $Qty){
			if (!empty($this->cart[$item_id])){
				$this->cart[$item_id] = $Qty;
				if($this->cart[$item_id] < 1){
					unset($this->cart[$item_id]);
				}
			}
		}
	
		function delItem($item_id, $Qty){
			if ($Qty < 1 or $Qty == 99999){
				unset($this->cart[$item_id]);
			}else{
				if (!empty($this->cart[$item_id])){
					$this->cart[$item_id] -= $Qty;
				}
			}
		}
	
		function getCartItems(){
			return count($this->cart);
		}
	
		function EmptyCart(){
			unset($this->cart);
		}
	
		function table_exists($table, $db) { 
			
			$sql = sprintf("SHOW TABLES FROM %s", $db);
			$result = mysql_query($sql);
			
			if (!$result) {
				$this->DisplayError(4);
			}
			
			while ($row = mysql_fetch_row($result)) {
				 if ($row[0] == $table){
				 	return TRUE;
				 }
			}
			return FALSE;
		}

		private function SetTblDts($dbNm, $tblNm, $prdPrc, $prdNm, $prdId, $prdImg, $ImgDir){
			$this->DBName = $dbNm;
			$this->tblNm = $tblNm;
			$this->prodPrc = $prdPrc;
			$this->prodNm = $prdNm;
			$this->prodId = $prdId;
			$this->prodImg = $prdImg;
			$this->ImgDir = $ImgDir;
		}
		
		function toMoney($Amt){
			return number_format($Amt, 2, '.', ', ');
		}
		
		function ShowCartWtPrd(){
			$itemClass = ($this->getCartItems() > 2) ? "insideContMainArea2": "insideContMainArea";
			$plural = ($this->getCartItems() > 1)? 's': '' ;
			$Out = 
			'<div id="theOverlay">&nbsp; 
			  <table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
				  <td width="20%"><div align="right"><img name="" src="'.  $this->CSS  .'imgs/wait.gif" width="32" height="32" alt="" /></div></td>
				  <td width="50%"><div align="left">&nbsp;<strong>Updating...Please Wait</strong> </div></td>
				</tr>
			  </table>
			</div>
			<div id="Cart_Container">
				<div id="Car_top_bg"></div>
			  <div id="Cart_middle">
					 <div class="insideContQty"><strong>' . $this->getCartItems() . '</strong> item'. $plural .' in your cart
					 (<a href="'. $this->ChkOutPg  . '">View</a> | <a href="#" onclick="if(confirm(\'Are you sure?\')){doCart(\'EmptyCart\', \'' . 0 . '\', 0,\'Small\');} ">Empty</a>) </div>
					 <div id="'. $itemClass. '">
					   <table width="100%" border="0" cellspacing="2" cellpadding="2">
						 <tr bgcolor="#E6EDD2">
						   <td width="58%" height="21" bgcolor="#E6EDD2"><div align="center"><strong>Item Description </strong></div></td>
						   <td width="11%"><div align="center"><strong>Qty</strong></div></td>
						   <td width="20%"><div align="center"><strong>Prc/Qty</strong></div></td>
						   <td width="11%"><div align="center"><strong>Del</strong></div></td>
						 </tr>' ;
						 $cnt=1;
						 foreach($this->cart as $itemId=>$Qty){
						 	++$cnt;
							$ans = fmod($cnt,2);
							if($ans == 0){$bg = '#ECF9FF';}else{$bg='';};
							$ProdDts = $this->getProdDts($itemId);
							
							$this->totAmt += $ProdDts[$this->prodPrc] * $Qty ;
							
								$Out .= '<tr bgcolor="'. $bg .'">
								<td valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="0">
									 <tr>
									   <td width="40%" valign="top"><img src="' . $this->ImgDir . $ProdDts[$this->prodImg] . '" alt="" width="45" height="47" hspace="3" vspace="3" /></td>
									   <td width="60%" valign="top">' . $ProdDts[$this->prodNm]  .' <br />                   </td>
									 </tr>
								   </table></td>
								   <td valign="top"><div align="center">' . $Qty .'</div></td>
								   <td valign="top">' . $ProdDts[$this->prodPrc]  .'</td>
								   <td valign="top"><div align="center"><a href="#" onclick="doCart(\'DelItem\', \'' . $ProdDts[$this->prodId] . '\', 0, \'Small\');"><img src="'.  $this->CSS  .'imgs/cart_remove.png" alt="Delete Item" width="16" height="16" border="0" title="Delete Item"/></a></div></td>
								 </tr>
								 <tr>
						   <td colspan="4" valign="top"><div class="clear"></div></td>
						 </tr>';
						 }
						 
						$Out .= '
					   </table> 
					 </div>
			  </div>
				<div id="Cart_gtotal">
					<div class="theCont">Grand Total => '. $this->ShowCurSymbol() .' '. $this->toMoney($this->totAmt) . '</div>
				</div>
			</div>'	;	
			
			echo $Out;
		}
		
		function DisplayCheckOut(){
			$plural = ($this->getCartItems() > 1)? 's': '' ;
			$Chk = '
				<form name="cart_product_form" id="car_product_submit_form" action="' . $this->ProcessingPg .'"" 
					
						method="post">
					<div id="theOverlay">&nbsp; 
					  <table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
						  <td width="20%"><div align="right"><img name="" src="'.  $this->CSS  .'imgs/wait.gif" width="32" height="32" alt="" /></div></td>
						  <td width="50%"><div align="left">&nbsp;<strong>Updating...Please Wait</strong> </div></td>
						</tr>
					  </table>
					</div>
					<div id="Cart_Container_DetailView">
						<div id="Car_top_bg"></div>
							 <div class="insideContQty"><strong>' . $this->getCartItems() . '</strong> item'. $plural .' in your cart <a href="#" class="block" onclick="if(confirm(\'Are you sure?\')){doCart(\'EmptyCart\', \'' . 0 . '\', 0, \'ChkOut\');} ">Empty Cart </a><br />
							  &nbsp;</div>
							 <div id="">
							   <table width="100%" border="0" cellspacing="2" cellpadding="2">
								 <tr bgcolor="#E6EDD2">
								   <td width="58%" height="21"><div align="center"><strong>Item Description </strong></div></td>
								   <td width="11%"><div align="center"><strong>Qty</strong></div></td>
								   <td width="20%"><div align="center"><strong>Price/Qty</strong></div></td>
								   <td width="11%"><div align="center"><strong>Del</strong></div></td>
								 </tr>';
								 foreach($this->cart as $itemId=>$Qty){
									$ProdDts = $this->getProdDts($itemId);
									$this->totAmt += $ProdDts[$this->prodPrc] * $Qty ;
								 
								 $Chk .='
								 <tr class="Angle90">
								   <td valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="0">
									 <tr>
									   <td width="40%"><img src="' . $this->ImgDir . $ProdDts[$this->prodImg] . '" alt="" width="120" height="120" hspace="3" vspace="3" /></td>
									   <td width="60%" valign="top">' . $ProdDts[$this->prodNm]  .'</td>
									 </tr>
								   </table></td>
								   <td valign="top"><div align="center"><input type="hidden" name="prcDz" value="'. $ProdDts[$this->prodPrc]  .'" id="prcDz_'. $itemId .'">
									 <input type="hidden" name="ItemId[]" value="' . $ProdDts[$this->prodId] .'" id="ItemId_'. $itemId  . '"/>
									 <input name="ItemQty[]" type="text" id="ItemQty" size="7" value="'. $Qty  .'" onkeypress="return isNumberKey(event); " />
								   </div></td>
								   <td valign="top" align="center">' . $this->ShowCurSymbol() .' '. $ProdDts[$this->prodPrc] .'<br /><br /></br /><span id="prcTot"></span></td>
								   <td valign="top"><div align="center"><a href="#" onclick="doCart(\'DelItem\', \'' . $ProdDts[$this->prodId] . '\', 0, \'ChkOut\');"><img src="'.  $this->CSS  .'/imgs/cart_remove.png" alt="Delete Item" width="24" height="24" border="0" title="Delete Item"/></a></div></td>
								 </tr>
								 <tr>
								   <td colspan="4" valign="top"><div class="clear"></div></td>
								 </tr>';
								 }
								 $Chk .='
								 <tr>
								   <td height="30" colspan="4" valign="top"><table width="100%" height="18" border="0" cellpadding="0" cellspacing="1">
									 <tr>
									   <td width="69%"><div align="right"><strong>Grand Total =&gt; </strong></div></td>
									   <td width="31%"><span class="theCont">'. $this->ShowCurSymbol() .' '. $this->toMoney($this->totAmt) .'</span></td>
									 </tr>
								   </table></td>
								 </tr>
								 <tr>
								   <td height="26" colspan="4" valign="top">
								   		<div align="center">
								   			<a href="#" class="block" 
								   				onclick="doCart(\'UpdCart\', \'ItemId[]\', \'ItemQty[]\',  \'ChkOut\');"
								   					>Update Cart Items</strong></a> </strong>
								   			<a href="#" onclick=" if(confirm(\'Are you sure you want to continue?\')){document.getElementById(\'car_product_submit_form\').submit();}"
								   			class="block" > Submit Order </a>
								   		</div>
								   	</td>
								 </tr>
							   </table> 
					  </div>
					</div>
				</form>';
				
				echo $Chk;
		}
		
		function DisplayEmptyCart($pg){
			if ($pg == 'ChkOut'){
				$classNm = "Cart_Container_DetailView";
			}
			else{
				$classNm = "Cart_Container";
			}
		
			if ($this->CurTyp == 'Naira'){
				$Cur = "<del>N</del>";
			}
			else{
				$Cur = "$";
			}
			
			$EmptyCart = 
			'<div id="theOverlay">&nbsp; 
			  <table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
				  <td width="20%"><div align="right"><img name="" src="'.  $this->CSS  .'imgs/wait.gif" width="32" height="32" alt="" /></div></td>
				  <td width="50%"><div align="left">&nbsp;<strong>Updating...Please Wait</strong> </div></td>
				</tr>
			  </table>
			</div>
				<div id="'. $classNm .'">
					<div id="Car_top_bg"></div>
				  <div id="Cart_middle2">
						 <div class="insideContQty">
								You have <strong>0</strong> item in your cart 
						</div>
								<div class="insideContEmpty">
								</div>
				  </div>
				  <div id="Cart_gtotal">
						<div class="theCont">Grand Total => '. $this->ShowCurSymbol() .' '. $this->toMoney($this->totAmt). '</div>
				  </div>
				</div>
			';
			echo $EmptyCart;
		}
		
		function getProdDts($id){
			$Qry = sprintf("SELECT * FROM %s WHERE %s ='%s' LIMIT 1",$this->tblNm, $this->prodId, $id);
			$theRes = $this->ExecQry($Qry) or die('Error geting product details..Resaon: '. mysql_error());
			$theRw = $this->getRow($theRes);
			return $theRw;
		}
		
		function DisplayRandProd($pCat){
			$Qry = sprintf("SELECT * FROM %s WHERE cat = '%s' LIMIT 5", $this->tblNm, $pCat);
			$theRes = $this->ExecQry($Qry);
			
			if (!$theRes){
				$this->DisplayError(4);
			}
			else{
				return $theRes;
			}
		}
				
		function getRow($QryRes){
			return mysql_fetch_assoc($QryRes);
		}

		function ExecQry($Qry){
			return mysql_query($Qry,$this->Conn);
		}

		function GetNumRec($recs){
			return mysql_num_rows($recs);
		}

		function destroyConn(){
			mysql_close($this->Conn);
		}
		
	}

	
?>
