-- phpMyAdmin SQL Dump
-- version 2.10.1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jan 21, 2010 at 04:15 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `cart`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `tblprod`
-- 

DROP TABLE IF EXISTS `tblprod`;
CREATE TABLE `tblprod` (
  `sn` int(11) NOT NULL auto_increment,
  `manufacturer` varchar(255) NOT NULL,
  `prodname` varchar(255) NOT NULL,
  `prodprc` varchar(255) NOT NULL,
  `prodid` varchar(5) NOT NULL,
  `status` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL,
  `cat` varchar(50) NOT NULL,
  PRIMARY KEY  (`sn`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

-- 
-- Dumping data for table `tblprod`
-- 

INSERT INTO `tblprod` (`sn`, `manufacturer`, `prodname`, `prodprc`, `prodid`, `status`, `image`, `cat`) VALUES 
(1, 'SanDisk', 'SanDisk Sansa Clip 2GB MP3 Player with FM ', '23.00', '123', 'Refurbished', 'SanDisk_SansaClip2gb.jpg', 'Ipods'),
(2, 'SanDisk', 'SanDisk Sansa Clip 4GB MP3 Player with FM', '32.07', '124', 'Refurbished', 'SanDisk_Sansa_Clip_4GB.jpg', 'Ipods'),
(3, 'Apple', 'Apple iPod nano 8GB 5th Generation Black ', '124.99', '125', 'Refurbished', 'Apple_iPod_nano_8GB.jpg', 'Ipods'),
(4, 'Aim', 'Aim 1GB MP3 Player /FM Tuner / Voice Recorder', '15.99', '126', 'New', 'Aim_1GB_MP3_Player_FM_Tuner_Voice Recorder.jpg', 'Ipods'),
(5, 'Philips', 'Philips 4GB Flash Audio Video MP3 Player', '33.49', '127', 'Refurbished', 'Philips_4GB_Flash_Audio_Video_MP3_Player.jpg', 'Ipods'),
(6, 'Latte', 'Latte Black 8 GB iPearl Music/Video Player/FM Transmitter', '127.89', '128', 'New', 'Latte_Black_8_GB_iPearl_Music_Video_Player_FM_Transmitter.jpg', 'Ipods'),
(7, 'SanDisk', 'SanDisk Sansa C250 2GB Multimedia MP3 Player Refurbished', '26.99', '129', 'Refurbished', 'SanDisk_Sansa_C250_2GB_Multimedia_MP3_Player.jpg', 'Ipods'),
(8, 'Apple', 'Apple iPod Shuffle 2GB 3rd Generation Black ', '49.99', '130', 'Refurbished', 'Apple_iPod_Shuffle_2GB_3rd_Generation_Black.jpg', 'Ipods'),
(9, 'Conet', 'Conet 1.5-inch LCD White 256MB MP4 Player', '17.99', '131', 'New', 'Conet_1.5-inch_LCD_White_256MB_MP4_Player.jpg', 'Ipods'),
(10, 'Latte', 'Latte Espresso 4 GB Music/ Video Player/ FM Transmitter', '69.99', '132', 'New', 'Latte_Espresso_4_GB_Music_Video_Player_FM_Transmitter.jpg', 'Ipods'),
(11, 'Nextar', 'Nextar MA852-4BL 4GB Digital MP3/MP4 Player with Voice Recorder', '34.49', '133', 'New', 'Nextar_MA852-4BL_4GB_Digital_MP3_MP4_Player_with_Voice_Recorder.jpg', 'Ipods'),
(12, 'Philips', 'Philips SA3025 2GB Flash Video MP3 Player/ FM Radio', '29.49', '134', 'Refurbished', 'Philips_SA3025_2GB_Flash_Video_MP3_Player_FM_Radio.jpg', 'Ipods'),
(13, 'Toshiba', 'Toshiba Satellite PSAG0U-02D00M A305-S6858 Laptop ', '809.99', '135', 'Refurbished', 'Toshiba_Satellite_PSAG0U-02D00M_A305-S6858_Laptop.jpg', 'Laptop'),
(14, 'Sony', 'Sony VAIO VGN-FW290JTH Laptop ', '944.00', '136', 'Refurbished', 'Sony_VAIO_VGN-FW290JTH_Laptop.jpg', 'Laptop'),
(15, 'Hp', 'HP Mini 110-1030NR 160GB 10.1-inch Netbook', '279.99', '137', 'Refurbished', 'HP_Mini_110-1030NR_160GB_10.1-inch_Netbook.jpg', 'Laptop'),
(16, 'Eee PC', 'Eee PC 1005HA-VU1X-WT White Seashell Netbook', '314.99', '138', 'New', 'Eee_PC_1005HA-VU1X-WT_White_Seashell_Netbook.jpg', 'Laptop'),
(17, 'Compaq', 'Compaq Presario CQ61-420US Laptop', '549.99', '139', 'New', 'Compaq_Presario_CQ61-420US_Laptop.jpg', 'Laptop'),
(18, 'Lenovo', 'Lenovo G550 Notebook', '516.63', '140', 'New', 'Lenovo_G550_Notebook.jpg', 'Laptop'),
(19, 'Acer', 'Acer Aspire One D250-1325 Netbook', '303.15', '141', 'New', 'Acer_Aspire_One_D250-1325_Netbook.jpg', 'Laptop'),
(20, 'LG', 'LG KF750 Secret GSM Unlocked Cell Phone', '188.49', '142', 'New', 'LG_KF750_Secret_GSM_Unlocked_Cell_Phone.jpg', 'Phone'),
(21, 'Nokia', 'Nokia 5300 Slider Purple/ White Unlocked GSM Cell Phone', '89.99', '143', 'New', 'Nokia_5300_Slider_Purple_White_Unlocked_GSM_Cell_Phone.jpg', 'Phone'),
(22, 'Samsung', 'Samsung F250 GSM Unlocked Cell Phone', '123.49', '144', 'New', 'Samsung_F250_GSM_Unlocked_Cell_Phone.jpg', 'Phone'),
(23, 'BlackBerry', 'BlackBerry Pearl 8100 Unlocked GSM Cell Phone', '169.99', '146', 'New', 'BlackBerry_Pearl_8100_Unlocked_GSM_Cell_Phone.jpg', 'Phone'),
(24, 'Motorola', 'Motorola Renew W233 Unlocked GSM Cell Phone', '46.99', '147', 'New', 'Motorola_Renew_W233_Unlocked_GSM_Cell_Phone.jpg', 'Phone'),
(25, 'LG', 'LG KP500 Cookie Quadband Unlocked GSM Touchscreen Cell Phone', '175.99', '148', 'New', 'LG_KP500_Cookie_Quadband_Unlocked_GSM_Touchscreen_Cell_Phone.jpg', 'Phone');
