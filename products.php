<?php

	if(!session_start()){
		session_start();
	}
	
	include("Config_IBKCart/Config.php");
	include("class_IBKCart/class.shopping.php");
	
	$Cart = new IBKCart();
	$Cart->initCart($Config, $_SESSION['MyCart']);
	
	switch($_GET['pCat']){
		case 'Laptop':
			$Qry = sprintf("SELECT * FROM tblprod WHERE cat = 'Laptop'");
		break;
		case 'Ipods':
			$Qry = sprintf("SELECT * FROM tblprod WHERE cat = 'Ipods'");
		break;
		case 'Phone':
			$Qry = sprintf("SELECT * FROM tblprod WHERE cat = 'Phone'");
		break;
	}
	
	$prdRes = $Cart->ExecQry($Qry) or die('Error running query. Reason: '. mysql_error());
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>IBK - Cart Class</title>
<style type="text/css">
<!--
body {
	font-family: Tahoma, Verdana, "Lucida Console";
	font-size: 12px;
}
body {
}
a:link {
	color: #333333;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #333333;
}
a:hover {
	text-decoration: none;
	color: #333333;
}
a:active {
	text-decoration: none;
	color: #333333;
}
.style1 {font-size: 24px}

.btn {
	background-color:#CCCCCC;
	padding:3px 5px 3px 5px;
}
body,td,th {
	color: #333333;
}
-->
</style>
<script type="text/javascript" src="js_IBKCart/ShoppingCart.js"></script>

<link href="CSS_IBKCart/Cart.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="800" border="0" align="center" cellpadding="2" cellspacing="0" style="border:1px dotted #666666;">
  <tr>
    <td><div align="center"><span class="style1">ShopOnline.com </span></div></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="30" colspan="2"><div align="center"><span class="block"><a href="index.php">Home</a></span> <span class="block"><a href="about.html">About</a></span>  <span class="block"><a href="doc.htm">Documentation</a></span></div></td>
      </tr>
      <tr>
        <td colspan="2"  style="border-bottom:1px dotted #666666;">&nbsp;</td>
      </tr>
      <tr>
        <td width="620">
		
<table width="620" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td><div align="center"><strong>Product Category: <?php echo $_GET['pCat']; ?></strong></div></td>
  </tr>
  <tr>
    <td class="clear"><div align="center" class="underlineMe"><strong>Product Brand: General </strong></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
  	$numRes = $Cart->GetNumRec($prdRes);
	if ($numRes > 0){
	
		while($prdRw = $Cart->getRow($prdRes)){
  ?>
  <tr>
    <td><table width="100%" border="0" cellpadding="2" cellspacing="0" class="Angle90" style="background-color:#FFFFFF;">
      <tr>
        <td width="23%" valign="top">
		<a href="#" onclick="doCart('AddItem', '<?php echo  $prdRw['prodid']; ?>', 1,'Small');">
		<img src="<?php echo $Config['imgDir'] . $prdRw['image']; ?>" width="120" height="120" alt="" style="padding:3px; border:1px solid #CCCCCC;"/>
		</a>
		</td>
        <td width="77%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2" style="color:#333333;">
          <tr>
            <td valign="top"><strong>Brand</strong></td>
            <td width="60%"><?php echo $prdRw['manufacturer'];  ?>&nbsp;</td>
            <td width="21%" align="right" style="vertical-align:middle;"><a href="#" style="text-decoration:none; color:#333333;" onclick="doCart('AddItem', '<?php echo $prdRw['prodid']; ?>', 1,'Small');"><img name="" src="imgs/plus.png" width="16" height="16" alt="" border="0"/> Add to Cart</a></td>
          </tr>
          <tr>
            <td width="19%" valign="top"><strong>Name</strong></td>
            <td><?php echo $prdRw['prodname'];  ?>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td height="23" valign="top"><strong>Price</strong></td>
            <td colspan="2" valign="top"><?php  echo $Cart->ShowCurSymbol($Config['Cur']) . $Cart->toMoney($prdRw['prodprc']);  ?>&nbsp;</td>
          </tr>
          <tr>
            <td height="23" valign="top"><strong>Status</strong></td>
            <td colspan="2" valign="top"><?php echo $prdRw['status'];  ?>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <?php
  		}
	?>
	
      <tr>
        <td valign="top" colspan="2"><div align="right"></div></td>
      </tr>
	 <?php
  	}
	else{
		//No product found..
  ?>
  <tr>
    <td>Sorry. No product was found for the category that you selected. </td>
  </tr>
  <?php
  	}
  ?>
</table>		
		
		</td>
        <td width="174" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr>
            <td><div id="IBKCart_Container">
              <?php if ($Cart->getCartItems() == 0){ $Cart->DisplayEmptyCart($a) ;} else{ echo $Cart->ShowCartWtPrd();}  ?>
            </div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<p>&nbsp; </p>
</body>
</html>
<?php $Cart->destroyConn(); ?>



