<?php
	if(!session_start()){
		session_start();
	}
		
	include("Config_IBKCart/Config.php");
	include("class_IBKCart/class.shopping.php");
	
	$Cart = new IBKCart();
	$Cart->initCart($Config, $_SESSION['MyCart']);
	
	//Lets load 3 random products for each level..
	$Laps = $Cart->DisplayRandProd('Laptop');
		while($LapRw = $Cart->getRow($Laps)){
			$LapArr[] = $LapRw;
		}
	$Phn = $Cart->DisplayRandProd('Phone');
		while($phnRw = $Cart->getRow($Phn)){
			$PhnArr[] = $phnRw;
		}
	$Ipd = $Cart->DisplayRandProd('Ipods');
		while($IpdRw = $Cart->getRow($Ipd)){
			$ipdArr[] = $IpdRw ;
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>IBK - Cart Class</title>
<style type="text/css">
<!--
body {
	font-family: Tahoma, Verdana, "Lucida Console";
	font-size: 12px;
}
body {
}
a:link {
	color: #333333;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #333333;
}
a:hover {
	text-decoration: none;
	color: #333333;
}
a:active {
	text-decoration: none;
	color: #333333;
}
.style1 {font-size: 24px}

.btn {
	background-color:#CCCCCC;
	padding:3px 5px 3px 5px;
}
body,td,th {
	color: #333333;
}
-->
</style>
<script type="text/javascript" src="js_IBKCart/ShoppingCart.js"></script>

<link href="CSS_IBKCart/Cart.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="800" border="0" align="center" cellpadding="2" cellspacing="0" style="border:1px dotted #666666;">
  <tr>
    <td><div align="center"><span class="style1">ShopOnline.com </span></div></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="30" colspan="2"><div align="center"><span class="block"><strong><a href="index.php">Home</a></strong></span> <span class="block"><a href="about.html">About</a></span> <span class="block"><a href="doc.htm">Documentation</a></span></div></td>
      </tr>
      <tr>
        <td colspan="2"  style="border-bottom:1px dotted #666666;">&nbsp;</td>
      </tr>
      <tr>
        <td width="649"><div style="border:1px dotted #666666; width:98%; padding:2px;">
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td colspan="3" class="style1"><div align="center">Available Products Categories.</div></td>
            </tr>
            <tr>
              <td width="34%" height="25"><div align="center" class="Angle90"><strong>Laptops</strong></div></td>
              <td width="33%"><div align="center" class="Angle90"><strong>Phones</strong></div></td>
              <td width="33%"><div align="center" class="Angle90"><strong>Ipods & Mp3s</strong></div></td>
            </tr>
			<?php
				$Used = array();
				for ($i=0; $i<3; $i++){
						
				?>
            <tr >
              <td class="Angle90"><table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr>
                  <td height="34" valign="middle"><div align="center"><a href="#" onclick="doCart('AddItem', '<?php echo $LapArr[$i][$Config['prodId']]; ?>', 1, 'Small');"><img src="<?php echo $Config['imgDir'] . $LapArr[$i][$Config['prodImg']] ; ?>" alt="" width="120" height="120" hspace="5" vspace="5" border="0" /></a><br />
                    <br /><strong><?php echo $LapArr[$i][$Config['prodNm']]; ?></strong>
                    <br /><?php echo $LapArr[$i]['status']; ?>
                    <br /><?php echo  $Cart->ShowCurSymbol($Config['Cur']) .'  ' . $Cart->toMoney($LapArr[$i][$Config['prodPrc']]); ?>
                  </div></td>
                </tr>
              </table></td>
              <td  class="Angle90"><table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr>
                  <td height="34" valign="middle"><div align="center"><a href="#" onclick="doCart('AddItem', '<?php echo $PhnArr[$i][$Config['prodId']]; ?>', 1, 'Small');"><img src="<?php echo $Config['imgDir'] . $PhnArr[$i][$Config['prodImg']] ; ?>" alt="" width="120" height="120" hspace="5" vspace="5" border="0" /></a><br />
                          <br />
                    <strong><?php echo $PhnArr[$i][$Config['prodNm']]; ?></strong> <br />
                    <?php echo $PhnArr[$i]['status']; ?> 
					<br /><?php echo $Cart->ShowCurSymbol($Config['Cur']) . ' '. $Cart->toMoney($PhnArr[$i][$Config['prodPrc']]); ?>
					</div></td>
                </tr>

              </table></td>
              <td  class="Angle90"><table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr>
                  <td height="34" valign="middle"><div align="center"><a href="#" onclick="doCart('AddItem', '<?php echo $ipdArr[$i][$Config['prodId']]; ?>', 1, 'Small');"><img src="<?php echo $Config['imgDir'] . $ipdArr[$i][$Config['prodImg']] ; ?>" alt="" width="120" height="120" hspace="5" vspace="5" border="0" /></a><br />
                          <br />
                          <strong><?php echo $ipdArr[$i][$Config['prodNm']]; ?></strong> <br />
                          <?php echo $ipdArr[$i]['status']; ?> 
						  <br /><?php echo $Cart->ShowCurSymbol($Config['Cur']) .'  ' . $Cart->toMoney($ipdArr[$i][$Config['prodPrc']]); ?>
						  </div></td>
                </tr>

              </table></td>
            </tr>
			<?php
					}
			?>
            <tr>
              <td height="34" valign="middle" class="rightBda"><div align="center"><a href="products.php?pCat=Laptop">View All Laptops </a><img name="" src="imgs/plus.png" width="16" height="16" alt="" /></div></td>
              <td height="34" valign="middle" class="rightBda"><div align="center"><a href="products.php?pCat=Phone">View All Phones </a><img name="" src="imgs/plus.png" width="16" height="16" alt="" /></div></td>
              <td height="34" valign="middle" class="rightBda"><div align="center"><a href="products.php?pCat=Ipods">View All Ipods and MP3s </a><img name="" src="imgs/plus.png" width="16" height="16" alt="" /></div></td>
            </tr>
          </table>
        </div></td>
        <td width="250" valign="top"><div id="IBKCart_Container"><?php if ($Cart->getCartItems() == 0){ $Cart->DisplayEmptyCart($a) ;} else{ echo $Cart->ShowCartWtPrd();}  ?></div></td>
      </tr>
    </table></td>
  </tr>
</table>
<p>&nbsp; </p>
</body>
</html>
<?php $Cart->destroyConn(); ?>