<?php
	
	$Config = array(
		'CSS'		=>	'CSS_IBKCart/',
		'imgDir' 	=>	'imgs/cartimgs/',	//Your images directory..Relative to your index page.
		'Cur'		=>	'Naira',	//Naira or Dollar
		'ChkOutPg'	=>	'prodCheckOut.php',		//Ensure this is the name of your checkout page and 
												//that it stays in the root directory as you index page..
		'ProcessingPg'	=> 'sampleProcessingPage.php', // The page where you want to do the 
														//necessary processing of the products in the cart, in same directory as index
		/*
			Db confguration
		*/
		
		'host'		=>	'localhost', 		//Change this to your host..
		'uNm'		=>	'root',  		//Change this to your username
		'pWd'		=>	'',		//Change this to your password..
		'dbNm'		=>	'cart',
		'prodTbl'	=>	'tblprod',	//The name of the table containing your product
		'prodPrc'	=>	'prodprc',	//The price column..
		'prodNm'	=>	'prodname',	//The name coulmn..
		'prodId'	=>	'prodid',	//The product id column..
		'prodImg'	=>	'image',	//Images column....

	);


?>